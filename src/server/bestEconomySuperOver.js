function bestEconomySuperOver(matches, deliveries){
        

    let validBall = 0;
    let balling = deliveries.reduce((bowlerName, ball) => {
        if(ball["is_super_over"] === '1'){
            if(bowlerName[ball["bowler"]]){
            if(ball["wide_runs"] == 0 && ball["noball_runs"] == 0){
                validBall = 1;
            }else{
                validBall = 0;
            }
        
            bowlerName[ball["bowler"]] += validBall;
        
        }else{
            if(ball["wide_runs"] == 0 && ball["noball_runs"] == 0){
                validBall = 1;
            }else{
                validBall = 0;
            }
            bowlerName[ball["bowler"]] = validBall;
        }
    }
        return bowlerName;
    }, {});


    let totalRunsGiven = deliveries.reduce((runsGiven, runs) =>{
        if(runs["is_super_over"] === '1'){
            if(runsGiven[runs["bowler"]]){
            runsGiven[runs["bowler"]] += parseInt(runs["total_runs"]);
        }else{
            runsGiven[runs["bowler"]] = parseInt(runs["total_runs"]);
        }
    }
        return runsGiven;
    }, {})


    let oversThrown = Object.entries(balling).reduce((perOver, over) => {
        perOver[over[0]] = over[1] / 6;
        return perOver;
    }, {})

    
    let count = 0;
    let bowlerEconomy = Object.entries(totalRunsGiven).reduce((runsPerOver, totalOver) => {
        runsPerOver[totalOver[0]] = totalOver[1] / Object.entries(oversThrown)[count++][1];
        return runsPerOver;
    }, {});


    let sortedEconomy = Object.entries(bowlerEconomy).sort((val1, val2) => {
        if(val1[1] < val2[1]){
            return -1;
        }
        if(val1[1] > val2[1]){
            return 1;
        }else{
            return 0;
        } 
    });
    return sortedEconomy.slice(0, 1);
} 

module.exports = bestEconomySuperOver;