function matchesWonPerTeam(matches){
    if(!matches){
        return {};
    }
    let wins = matches.reduce((years, winTeam) => {
        if(!years[winTeam["season"]]){
            years[winTeam["season"]] = {};
        }
        if(!years[winTeam["season"]][winTeam["winner"]]){
            years[winTeam["season"]][winTeam["winner"]] = 1;
        }else{
            years[winTeam["season"]][winTeam["winner"]] += 1;
        }
        return years;
    },{})
    return wins;
}

module.exports = matchesWonPerTeam;