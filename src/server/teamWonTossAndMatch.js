function wonTossAndMatch(matches){
    if(!matches){
        return {};
    }
    let teamWinning = 0;
    let tossWon = matches.reduce((element, teams) => {
        if(element[teams["winner"]]){
        if(teams["winner"] === teams["toss_winner"]){
            teamWinning = 1;
        }else{
            teamWinning = 0;
        }
        element[teams["winner"]] += teamWinning;
    }else{
        if(teams["winner"] === teams["toss_winner"]){
            teamWinning = 1;
        }else{
            teamWinning = 0;
        }
        element[teams["winner"]] = teamWinning;
    }
        return element;
        
    }, {});
    return tossWon;       
}

module.exports = wonTossAndMatch;