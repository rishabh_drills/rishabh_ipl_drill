function topEconomicBowlers2015(matches, deliveries, year = "2015"){
    const matchYear = matches.reduce((match, checkId) => {
        if(checkId["season"] === year){
            match.push(checkId["id"]);
        }
        return match;
    }, []);
// console.log(matchYear);

    const idPresent = deliveries.reduce((deliverId, delivery) =>{
        if(matchYear.includes(delivery["match_id"])){
            deliverId.push(delivery);
        }
        return deliverId;
    }, []);
// console.log(idPresent);

    let validBall = 0;
    let balling = idPresent.reduce((bowlerName, ball) => {
        if(bowlerName[ball["bowler"]]){
            if(ball["wide_runs"] == 0 && ball["noball_runs"] == 0){
                validBall = 1;
            }else{
                validBall = 0;
            }
            bowlerName[ball["bowler"]] += validBall;
        }else{
            if(ball["wide_runs"] == 0 && ball["noball_runs"] == 0){
                validBall = 1;
            }else{
                validBall = 0;
            }
            bowlerName[ball["bowler"]] = validBall;
        }
        return bowlerName;
    }, {});
 console.log(balling);

    let totalRunsGiven = idPresent.reduce((runsGiven, runs) =>{
        if(runsGiven[runs["bowler"]]){
            runsGiven[runs["bowler"]] += parseInt(runs["total_runs"]);
        }else{
            runsGiven[runs["bowler"]] = parseInt(runs["total_runs"]);
        }
        return runsGiven;
    }, [])
// console.log(totalRunsGiven);

    let oversThrown = Object.entries(balling).reduce((perOver, over) => {
        perOver[over[0]] = over[1] / 6;
        return perOver;
    }, {})
// console.log(oversThrown);
    
    let count = 0;
    let bowlerEconomy = Object.entries(totalRunsGiven).reduce((runsPerOver, totalOver) => {
        runsPerOver[totalOver[0]] = totalOver[1] / Object.entries(oversThrown)[count++][1];
        return runsPerOver;
    }, {});
// console.log(bowlerEconomy);

    let sortedEconomy = Object.entries(bowlerEconomy).sort((val1, val2) => {
        if(val1[1] < val2[1]){
            return -1;
        }
        if(val1[1] > val2[1]){
            return 1;
        }else{
            return 0;
        } 
    });
    return sortedEconomy.slice(0, 10);
} 

module.exports = topEconomicBowlers2015;