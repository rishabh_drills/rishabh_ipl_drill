function extraRunsPerTeam2016(matches, deliveries){
    if(!matches || !deliveries){
        return {};
    }
    const idCheck = matches.reduce((team, matchId) => {
        if(matchId["id"] >= 577 && matchId["id"] <= 636){
            team[matchId["id"]] = matchId["id"];
        }
        return team;
    },{});
    let extraRunsConceded = deliveries.reduce((runs, match) => {
        if(idCheck[match["match_id"]]){
            if(runs[match["bowling_team"]]){
                runs[match["bowling_team"]] += parseInt(match["extra_runs"]);
            }else{
            runs[match["bowling_team"]] = parseInt(match["extra_runs"]);
        }
    }
    return runs;
    }, {})
    return extraRunsConceded;
}


module.exports = extraRunsPerTeam2016;