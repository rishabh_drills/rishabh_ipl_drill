function highestPlayerOfTheMatch(matches){
    if(!matches){
        return {};
    }
    let manOfTheMatch = matches.reduce((match, player) =>{
        if(!match[player["season"]]){
            match[player["season"]] = {}; 
        }
        if(!match[player["season"]][player["player_of_match"]]){
            match[player["season"]][player["player_of_match"]] = 1
        }else{
            match[player["season"]][player["player_of_match"]] += 1
        }  
        return match;
    },{});

    let playerOfMatch = Object.entries(manOfTheMatch).reduce((acc, curr) =>{
        let topPlayerOfMatch = Object.entries(curr[1]).sort((val1, val2) =>{
            return val2[1] - val1[1];
        }).slice(0, 1);
        acc[curr[0]] = Object.fromEntries(topPlayerOfMatch);
        return acc;
    },{});
    return playerOfMatch;   
}


module.exports = highestPlayerOfTheMatch;