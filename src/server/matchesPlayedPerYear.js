function matchesPlayedPerYear(matches){
    if(!matches){
        return {};
    }
    const matchPlayedyear = matches.reduce((match, year) =>{
        if(!match[year["season"]]){
            match[year["season"]] = 1;
        }else{
            match[year["season"]] += 1
        }
        return match;
    },{})
    return matchPlayedyear;
}

module.exports = matchesPlayedPerYear;